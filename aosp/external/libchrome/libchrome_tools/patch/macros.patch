--- a/base/macros.h
+++ b/base/macros.h
@@ -12,19 +12,32 @@
 
 #include <stddef.h>  // For size_t.
 
+#if defined(ANDROID)
+// Prefer Android's libbase definitions to our own.
+#include <android-base/macros.h>
+#endif  // defined(ANDROID)
+
+// We define following macros conditionally as they may be defined by another libraries.
+
 // Put this in the declarations for a class to be uncopyable.
+#if !defined(DISALLOW_COPY)
 #define DISALLOW_COPY(TypeName) \
   TypeName(const TypeName&) = delete
+#endif
 
 // Put this in the declarations for a class to be unassignable.
+#if !defined(DISALLOW_ASSIGN)
 #define DISALLOW_ASSIGN(TypeName) \
   void operator=(const TypeName&) = delete
+#endif
 
 // A macro to disallow the copy constructor and operator= functions.
 // This should be used in the private: declarations for a class.
+#if !defined(DISALLOW_COPY_AND_ASSIGN)
 #define DISALLOW_COPY_AND_ASSIGN(TypeName) \
   TypeName(const TypeName&) = delete;      \
   void operator=(const TypeName&) = delete
+#endif
 
 // A macro to disallow all the implicit constructors, namely the
 // default constructor, copy constructor and operator= functions.
@@ -32,9 +45,11 @@
 // This should be used in the private: declarations for a class
 // that wants to prevent anyone from instantiating it. This is
 // especially useful for classes containing only static methods.
+#if !defined(DISALLOW_IMPLICIT_CONSTRUCTORS)
 #define DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName) \
   TypeName() = delete;                           \
   DISALLOW_COPY_AND_ASSIGN(TypeName)
+#endif
 
 // The arraysize(arr) macro returns the # of elements in an array arr.  The
 // expression is a compile-time constant, and therefore can be used in defining
@@ -45,8 +60,10 @@
 // This template function declaration is used in defining arraysize.
 // Note that the function doesn't need an implementation, as we only
 // use its type.
+#if !defined(arraysize)
 template <typename T, size_t N> char (&ArraySizeHelper(T (&array)[N]))[N];
 #define arraysize(array) (sizeof(ArraySizeHelper(array)))
+#endif
 
 // Used to explicitly mark the return value of a function as unused. If you are
 // really sure you don't want to do anything with the return value of a function
@@ -79,8 +96,10 @@ enum LinkerInitialized { LINKER_INITIALI
 // Use these to declare and define a static local variable (static T;) so that
 // it is leaked so that its destructors are not called at exit. If you need
 // thread-safe initialization, use base/lazy_instance.h instead.
+#if !defined(CR_DEFINE_STATIC_LOCAL)
 #define CR_DEFINE_STATIC_LOCAL(type, name, arguments) \
   static type& name = *new type arguments
+#endif
 
 }  // base
 
