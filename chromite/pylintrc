# Copyright (c) 2011 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

[MASTER]

# Specify a configuration file.
#rcfile=

# Python code to execute, usually for sys.path manipulation such as
# pygtk.require().
#init-hook=

# Profiled execution.
#profile=no

# Add <file or directory> to the black list. It should be a base name, not a
# path. You may set this option multiple times.
#ignore=CVS

# Pickle collected data for later comparisons.
#persistent=yes

# List of plugins (as comma separated values of python modules names) to load,
# usually to register additional checkers.
load-plugins=chromite.cli.cros.lint


[MESSAGES CONTROL]

# Enable the message, report, category or checker with the given id(s). You can
# either give multiple identifier separated by comma (,) or put this option
# multiple times.
# R9301: logging is deprecated. Use "from chromite.lib import cros_logging as
# logging" to import chromite/lib/cros_logging
enable=
    cros-logging-import
    apply-builtin,
    backtick,
    bad-python3-import,
    buffer-builtin,
    cmp-builtin,
    cmp-method,
    coerce-builtin,
    coerce-method,
    delslice-method,
    deprecated-itertools-function,
    deprecated-str-translate-call,
    deprecated-string-function,
    deprecated-types-field,
    dict-items-not-iterating,
    dict-iter-method,
    dict-keys-not-iterating,
    dict-values-not-iterating,
    dict-view-method,
    div-method,
    exception-message-attribute,
    execfile-builtin,
    file-builtin,
    filter-builtin-not-iterating,
    getslice-method,
    hex-method,
    idiv-method,
    import-star-module-level,
    indexing-exception,
    input-builtin,
    intern-builtin,
    invalid-str-codec,
    long-builtin,
    next-method-defined,
    nonzero-method,
    oct-method,
    old-raise-syntax,
    parameter-unpacking,
    print-statement,
    raising-string,
    range-builtin-not-iterating,
    raw_input-builtin,
    rdiv-method,
    reload-builtin,
    setslice-method,
    standarderror-builtin,
    sys-max-int,
    unichr-builtin,
    unpacking-in-except,
    using-cmp-argument,
    xrange-builtin,
    zip-builtin-not-iterating,


# Disable the message, report, category or checker with the given id(s). You
# can either give multiple identifiers separated by comma (,) or put this
# option multiple times (only on the command line, not in the configuration
# file where it should appear only once). You can also use "--disable=all" to
# disable everything first and then reenable specific checks. For example, if
# you want to run only the similarities checker, you can use "--disable=all
# --enable=similarities". If you want to run only the classes checker, but have
# no Warning level messages displayed, use "--disable=all --enable=classes
# --disable=W".
# We leave many of the style warnings to judgement/peer review.
# TODO: We need to re-enable broad-except, but requires cleaning up our code.
# TODO: Re-enable wrong-import-order/wrong-import-position.
# TODO: Re-enable redefined-variable-type.
disable=
    broad-except,
    consider-iterating-dictionary,
    fixme,
    file-ignored,
    invalid-name,
    locally-disabled,
    locally-enabled,
    missing-docstring,
    no-member,
    no-self-use,
    redefined-variable-type,
    relative-import,
    too-few-public-methods,
    too-many-arguments,
    too-many-boolean-expressions,
    too-many-branches,
    too-many-instance-attributes,
    too-many-lines,
    too-many-locals,
    too-many-nested-blocks,
    too-many-public-methods,
    too-many-return-statements,
    too-many-statements,
    wrong-import-order,
    wrong-import-position,


[REPORTS]

# Set the output format. Available formats are text, parseable, colorized, msvs
# (visual studio) and html
#output-format=text

# Put messages in a separate file for each module / package specified on the
# command line instead of printing them on stdout. Reports (if any) will be
# written in a file name "pylint_global.[txt|html]".
#files-output=no

# Tells whether to display a full report or only the messages
# CHANGE: No report.
reports=no

# Python expression which should return a note less than 10 (10 is the highest
# note). You have access to the variables errors warning, statement which
# respectively contain the number of errors / warnings messages and the total
# number of statements analyzed. This is used by the global evaluation report
# (RP0004).
#evaluation=10.0 - ((float(5 * error + warning + refactor + convention) / statement) * 10)

# Add a comment according to your evaluation note. This is used by the global
# evaluation report (RP0004).
#comment=no


[MISCELLANEOUS]

# List of note tags to take in consideration, separated by a comma.
#notes=FIXME,XXX,TODO


[FORMAT]

# Maximum number of characters on a single line.
max-line-length=80

# Maximum number of lines in a module
#max-module-lines=1000

# String used as indentation unit. This is usually " " (4 spaces) or "\t" (1
# tab).
# CHANGE: Use "  " instead.
indent-string='  '


[TYPECHECK]

# Tells whether missing members accessed in mixin class should be ignored. A
# mixin class is detected if its name ends with "mixin" (case insensitive).
#ignore-mixin-members=yes

# List of classes names for which member attributes should not be checked
# (useful for classes with attributes dynamically set).
ignored-classes=hashlib,numpy

# When zope mode is activated, add a predefined set of Zope acquired attributes
# to generated-members.
#zope=no

# List of members which are set dynamically and missed by pylint inference
# system, and so shouldn't trigger E0201 when accessed.
# CHANGE: Added 'AndRaise', 'AndReturn', 'InAnyOrder' and 'MultipleTimes' for pymox.
# CHANGE: Added tempdir for @osutils.TempDirDecorator.
generated-members=REQUEST,acl_users,aq_parent,AndRaise,AndReturn,InAnyOrder,MultipleTimes,tempdir


[BASIC]

# Required attributes for module, separated by a comma
#required-attributes=

# List of builtins function names that should not be used, separated by a comma.
# XXX: Should we ban map() for list comprehensions?
# exit & quit are for the interactive interpreter shell only.
# https://docs.python.org/3/library/constants.html#constants-added-by-the-site-module
bad-functions=
    apply,
    exit,
    filter,
    input,
    quit,
    raw_input,

# Regular expression which should only match correct module names
#module-rgx=(([a-z_][a-z0-9_]*)|([A-Z][a-zA-Z0-9]+))$

# Regular expression which should only match correct module level names
#const-rgx=(([A-Z_][A-Z0-9_]*)|(__.*__))$

# Regular expression which should only match correct class names
#class-rgx=[A-Z_][a-zA-Z0-9]+$

# Regular expression which should only match correct function names
#
# CHANGE: The ChromiumOS standard is different than PEP-8, so we need to
# redefine this.
#
# Common exceptions to ChromiumOS standard:
# - main: Standard for main function
function-rgx=([A-Z_][a-zA-Z0-9]{2,30}|main)$

# Regular expression which should only match correct method names
#
# CHANGE: The ChromiumOS standard is different than PEP-8, so we need to
# redefine this. Here's what we allow:
# - CamelCaps, starting with a capital letter.  No underscores in function
#   names.  Can also have a "_" prefix (private method) or a "test" prefix
#   (unit test).
# - Methods that look like __xyz__, which are used to do things like
#   __init__, __del__, etc.
# - setUp, tearDown: For unit tests.
method-rgx=((_|test)?[A-Z][a-zA-Z0-9]{2,30}|__[a-z]+__|setUp|tearDown)$

# Regular expression which should only match correct instance attribute names
#attr-rgx=[a-z_][a-z0-9_]{2,30}$

# Regular expression which should only match correct argument names
#argument-rgx=[a-z_][a-z0-9_]{2,30}$

# Regular expression which should only match correct variable names
#variable-rgx=[a-z_][a-z0-9_]{2,30}$

# Regular expression which should only match correct list comprehension /
# generator expression variable names
#inlinevar-rgx=[A-Za-z_][A-Za-z0-9_]*$

# Good variable names which should always be accepted, separated by a comma
#good-names=i,j,k,ex,Run,_

# Bad variable names which should always be refused, separated by a comma
#bad-names=foo,bar,baz,toto,tutu,tata

# Regular expression which should only match functions or classes name which do
# not require a docstring
#no-docstring-rgx=__.*__


[SIMILARITIES]

# Minimum lines number of a similarity.
min-similarity-lines=20

# Ignore comments when computing similarities.
#ignore-comments=yes

# Ignore docstrings when computing similarities.
#ignore-docstrings=yes


[VARIABLES]

# Tells whether we should check for unused import in __init__ files.
#init-import=no

# A regular expression matching the beginning of the name of dummy variables
# (i.e. not used).
dummy-variables-rgx=_|unused_

# List of additional names supposed to be defined in builtins. Remember that
# you should avoid to define new builtins when possible.
#additional-builtins=


[CLASSES]

# List of interface methods to ignore, separated by a comma. This is used for
# instance to not check methods defines in Zope's Interface base class.
#ignore-iface-methods=isImplementedBy,deferred,extends,names,namesAndDescriptions,queryDescriptionFor,getBases,getDescriptionFor,getDoc,getName,getTaggedValue,getTaggedValueTags,isEqualOrExtendedBy,setTaggedValue,isImplementedByInstancesOf,adaptWith,is_implemented_by

# List of method names used to declare (i.e. assign) instance attributes.
#defining-attr-methods=__init__,__new__,setUp


[DESIGN]

# Maximum number of arguments for function / method
#max-args=5

# Argument names that match this expression will be ignored. Default to name
# with leading underscore
#ignored-argument-names=_.*

# Maximum number of locals for function / method body
#max-locals=15

# Maximum number of return / yield for function / method body
#max-returns=6

# Maximum number of branch for function / method body
#max-branchs=12

# Maximum number of statements in function / method body
#max-statements=50

# Maximum number of parents for a class (see R0901).
max-parents=10

# Maximum number of attributes for a class (see R0902).
#max-attributes=7

# Minimum number of public methods for a class (see R0903).
#min-public-methods=2

# Maximum number of public methods for a class (see R0904).
#max-public-methods=20


[IMPORTS]

# Deprecated modules which should not be used, separated by a comma.
# Bastion: Dropped in Python 3.
# mox: Use the 'mock' module instead.
# optparse: Use the 'argparse' module instead.
# regsub: Use the 're' module instead.
# rexec: Dropped in Python 3.
# TERMIOS: Use the 'termios' module instead.
deprecated-modules=
    Bastion,
    mox,
    optparse,
    regsub,
    rexec,
    TERMIOS,

# Create a graph of every (i.e. internal and external) dependencies in the
# given file (report RP0402 must not be disabled)
#import-graph=

# Create a graph of external dependencies in the given file (report RP0402 must
# not be disabled)
#ext-import-graph=

# Create a graph of internal dependencies in the given file (report RP0402 must
# not be disabled)
#int-import-graph=


[LOGGING]

# Apply logging string format checks to calls on these modules.
logging-modules=
    logging,
    chromite.lib.cros_logging,
