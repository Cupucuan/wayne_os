# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Start Chrome OS camera service"
author        "chromium-os-dev@chromium.org"

start on starting system-services
stop on stopping system-services
expect fork
respawn
respawn limit 5 30

env SECCOMP_POLICY_FILE=/usr/share/policy/cros-camera.policy
env UVC_HWTIMESTAMPS_FILE=/sys/module/uvcvideo/parameters/hwtimestamps

# Don't respawn too aggressively so kernel has some room to breathe and
# initialize cameras.
env RESPAWN_DELAY=3

# cros_camera_service provides camera service to both Chrome and ARC++:
# Chrome hosts the CameraHalDispatcher Mojo proxy; cros_camera_service forks a
# child process to host the camera HAL adapter and registers to
# CameraHalDispatcher Mojo proxy.  Camera clients can then ask
# CameraHalDispatcher Mojo proxy to get the handle to connect to the camera HAL
# adapter.

pre-start script
  # Run the board-specific setup hooks, if any.
  sh /etc/camera/setup-hooks.sh || true
  [ -e "${UVC_HWTIMESTAMPS_FILE}" ] && echo 1 > "${UVC_HWTIMESTAMPS_FILE}" || true
end script

post-stop script
  sleep "${RESPAWN_DELAY}"
end script

# Need -G to be in video group to access /dev/video*.
# Need access to original network namespace for udev (no -e), which passes
# uevents via netlink socket.
# Need access to /proc, /sys, /dev to perform USB enumeration.
# Need access to /var to read camera parameters.
# Need (writable) access to /dev/shm to share memory with Android.
# Need (writable) access to /sys/kernel/debug/tracing to log events.
# Recursively bind-mount /run into /var/run (MS_BIND|MS_REC=0x5000)
# Set RLIMIT_NICE(=13) to 40,40
exec minijail0 -i -u arc-camera -g arc-camera -G \
    -N -p -r -l -t \
    -v -P /var/empty -b / -b /proc -b /sys -b /dev -b /var -b /dev/shm,,1 \
    -b /sys/kernel/debug -b /sys/kernel/debug/tracing,,1 \
    -k tmpfs,/run,tmpfs,0xe \
    -b /run/camera,,1 -b /run/udev/data \
    -k /var/empty/run,/var/run,bind,0x5000 \
    -R 13,40,40 \
    -n -S "${SECCOMP_POLICY_FILE}" \
    -- /usr/bin/cros_camera_service
