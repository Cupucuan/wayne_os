Chrome OS is missing or damaged.
Please insert a recovery USB stick or SD card.
Please insert a recovery USB stick.
Please insert a recovery SD card or USB stick (note: the blue USB port will NOT work for recovery).
Please insert a recovery USB stick into one of the 4 ports in the BACK of the device.
The device you inserted does not contain Chrome OS.
OS verification is OFF
Press SPACE to re-enable.
Press ENTER to confirm you wish to turn OS verification on.
Your system will reboot and local data will be cleared.
To go back, press ESC.
OS verification is ON.
To turn OS verification OFF, press ENTER.
For help visit https://google.com/chromeos/recovery
Error code
Please remove all external devices to begin recovery.
Model 60061e
To turn OS verification OFF, press the RECOVERY button.
The connected power supply does not have enough power to run this device.
Chrome OS will now shut down.
Please use the correct adapter and try again.
Please remove all connected devices and start recovery.
Press a numeric key to select an alternative bootloader:
Press the POWER button to run diagnostics.
To turn OS verification OFF, press the POWER button.
