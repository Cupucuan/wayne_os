Chrome OS ગુમ છે અથવા ક્ષતિગ્રસ્ત છે.
કૃપા કરીને એક પુનઃપ્રાપ્તિ USB સ્ટિક અથવા SD કાર્ડને શામેલ કરો.
કૃપા કરીને એક પુનઃપ્રાપ્તિ USB સ્ટિકને શામેલ કરો.
કૃપા કરીને એક પુનઃપ્રાપ્તિ SD કાર્ડ અથવા USB સ્ટિકને શામેલ કરો. (નોંધ: વાદળી USB પોર્ટ પુનઃપ્રાપ્તિ માટે કાર્ય કરશે નહીં).
કૃપા કરીને ઉપકરણની પાછળના 4 પોર્ટમાંથી એકમાં પુનઃપ્રાપ્તિ USB સ્ટિકને શામેલ કરો.
તમે શામેલ કરેલ ઉપકરણમાં Chrome OS નથી.
OSની ચકાસણી બંધ છે
તેને ફરી સક્ષમ કરવા SPACE દબાવો.
OSની ચકાસણીને ચાલુ કરવા માગો છો તેની પુષ્ટિ કરવા માટે ENTER દબાવો.
તમારી સિસ્ટમ રીબૂટ થશે અને સ્થાનિક ડેટા સાફ કરવામાં આવશે.
પાછળ જવા માટે, ESC દબાવો.
OSની ચકાસણી ચાલુ છે.
OSની ચકાસણી બંધ કરવા માટે, ENTER દબાવો.
સહાય માટે https://google.com/chromeos/recoveryની મુલાકાત લો
ભૂલ કોડ
પુનઃપ્રાપ્તિ શરૂ કરવા માટે કૃપા કરીને બધા બાહ્ય ઉપકરણો દૂર કરો.
મોડેલ 60061e
OSની ચકાસણી બંધ કરવા માટે, RECOVERY બટનને દબાવો.
કનેક્ટ કરેલ પાવર સપ્લાયમાં આ ઉપકરણને શરૂ કરવા માટે પૂરતો પાવર નથી.
Chrome OS હવે બંધ થઈ જશે.
કૃપા કરીને યોગ્ય ઍડેપ્ટરનો ઉપયોગ કરો અને ફરી પ્રયાસ કરો.
કૃપા કરીને કનેક્ટ કરેલા બધા ઉપકરણો દૂર કરો અને પુનઃપ્રાપ્તિ શરૂ કરો.
વૈકલ્પિક બૂટલોડર પસંદ કરવા માટે આંકડાકીય કી દબાવો:
નિદાનની સુવિધા ચાલુ કરવા માટે પાવર બટનને દબાવો.
OS ચકાસણી બંધ કરવા માટે, પાવર બટન દબાવો.
