# Sitemap

## Getting Started

*   [Getting Started Quickly](./getting_started_quickly.md)
*   [Core Runtime](./core_runtime.md)
*   [Write Protection](./write_protection.md)

## Case Closed Debugging (CCD)

*   [Case Closed Debugging Overview](./case_closed_debugging.md)
*   [Google Security Chip Case Closed Debugging](./case_closed_debugging_cr50.md)

## Fingerprint MCU (FPMCU)

*   [Fingerprint MCU (FPMCU)](./fingerprint/fingerprint.md)

## Updaters

*   [USB Updater](./usb_updater.md)

## USB-C

*   [USB-C Power Delivery and Alternate Modes](./usb-c.md)

## Miscellaneous

*   [Low Battery Startup](./low_battery_startup.md)
*   [I2C tracing via console commands](./i2c-debugging.md)
