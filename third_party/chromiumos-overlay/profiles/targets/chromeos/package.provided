# Copyright (c) 2009 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

# This file lists packages that ebuilds DEPEND on, sometimes implicitly,
# but really are only needed on the build host. This allows us to use
# --root-deps without "--rootdeps=rdeps" to install package DEPEND into
# the sysroot as part of the build process without also having to cross-
# compile and drag in the below packages as dependencies. See "man portage".

# NOTE: Toolchain packages (gcc, glibc, binutils) are specified in the
# dynamically generated ${BOARD_DIR}/etc/portage/profile/package.provided
# created by the setup_board script.

app-admin/eselect-1.2.9
app-admin/eselect-esd-20060719
app-admin/eselect-opengl-1.0.8-r1
app-admin/eselect-mesa-0.0.8
app-admin/eselect-vi-1.1.5

app-arch/cabextract-1.2-r1
app-arch/rpm2targz-9.0.0.3g

# We don't need xmltoman in the sysroot, so pick a large version as they
# should all be compatible.
app-doc/xmltoman-1.0

# Needed for libtool.eclass.
app-portage/elt-patches-20170815

app-text/build-docbook-catalog-1.4
app-text/docbook-xsl-stylesheets-1.75.2
app-text/texi2html-5.0

dev-lang/nasm-2.07

# Needed for building Icedtea
dev-java/ant-core-1.7.1-r4
dev-java/xalan-2.7.1
dev-java/xerces-2.9.1

dev-lang/perl-5.24.0-r2

dev-perl/Crypt-PasswdMD5-1.3
dev-perl/Digest-SHA1-2.11
dev-perl/XML-Parser-2.36

dev-util/cmake-3.9.6
dev-util/ctags-5.7
dev-util/gperf-3.0.3
dev-util/gtk-doc-1.13-r3
dev-util/gtk-doc-am-1.13-r2
dev-util/ninja-1.8.2
dev-util/pkgconfig-0.23

perl-core/digest-base-1.16
perl-core/File-Temp-0.230.400-r1
perl-core/MIME-Base64-3.08

sys-apps/help2man-1.36.4

# Needed for building Icedtea
sys-apps/lsb-release-1.4

sys-apps/texinfo-4.13

sys-devel/autoconf-2.63-r1
sys-devel/automake-1.10.2
sys-devel/bison-2.3
sys-devel/gettext-0.19.9999
sys-devel/gnuconfig-20090203
sys-devel/m4-1.4.12

sys-kernel/gentoo-sources-2.6.30-r6

x11-apps/mkfontscale-1.0.6
x11-misc/makedepend-1.0.1

# Legacy font map encodings which we don't care about.  http://crosbug.com/25001
media-fonts/encodings-1.0.3

# Our chromeos-base package takes care of this.
app-misc/editor-wrapper-4

# A build tool only; not needed on the target board.
dev-util/boost-build-1.65.0

# A build tool only; not needed on the target board.
dev-util/cargo-0.29.0

# We don't need the toolchain itself in the target board.
dev-lang/rust-1.36.0
dev-lang/rust-1.35.0

# We sometimes need ruby for building, but don't yet care about targets.
dev-lang/ruby-1.9.4
dev-ruby/rubygems-2.2.2

# Needed for xorg-proto.
dev-util/meson-0.44.1

# app-admin/sudo-1.8.23-r2 depends on virtual/tmpfiles. sudo only actually
# needs this facility on systems using SELinux. Provide a non-virtual package
# to avoid confusing warnings.
sys-apps/opentmpfiles-0.2

# Test suites are executed on the SDK's python interpreter, so boards don't need
# their own copy of pytest.
dev-python/pytest-4.4
