Release notes for Cyapa_fw_update utiltiy tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
For instructions on how to build/install/use it, see the end of this file.


Executive Summary
~~~~~~~~~~~~~~~~~
Cyapa_fw_update utility tool is designed for updating Cypress Trackpad
device firmware image in user space with .iic or .bin firmware file.

Cyapa_fw_upate is licensed under the BSD 2-Clause License.
Read the file COPYING in the source distribution for details.

However: if you contribute code, you need to make it available as
BSD 2-Clause License or later, and not 2-only.


Documentation
~~~~~~~~~~~~~

Building and installing it
~~~~~~~~~~~~~~~~~~~~~~~~~~
To build the cyapa_fw_update tool:

  0. Copy cyapa.h and cyapa_fw_update.c to your compiling system.

  1. Compile cyapa_fw_update utility tool for the target device as below
        $GCC -Wall -o cyapa_fw_update cyapa_fw_update.c

     e.g.: for ARM tegra2 platfrom, the cross gcc tool for GCC is defined
           as "armv7a-cros-linux-gnueabi-gcc"

  2. Copy to the target device.


Using it
~~~~~~~~
Before using this cyapa_fw_update utiltiy tool, firstly must satify
below conditions:
  1. The cyapa_fw_update utility tool has been compiled for the system;

  2. Have got the correct Cypress Trackpad device firmware image.
     The valid firmware image file for Cypress Trackpad devices are those
     that have .iic or .bin extension files.

  3. Must have root privilege to run this utility tool.

Then, can use below commands to update Cypress Trackpad device firmware:
  1. Update firmware image with prompt information:
      # ./cyapa_fw_update <new_image.iic>

  2. Update firmware image eithout prompt information, supresss prompt:
      # ./cyapa_fw_update <new_image.iic> -f

  3. Store old firmware from trackpad device before updating new firmware image:
     1). Store old firmware image to defalut backup firmare image file:
              # ./cyapa_fw_update <new_image.iic> -b

         The default backup firmware image file is
         "/tmp/cypress/cyapa_bak_firmware.bin".

     2). Store old firmware image to your specific path and file:
              # ./cyapa_fw_update <new_inage.iic> -b -o <your-backup-file.bin>

         NOTE: the output image file name must have .bin extension.


