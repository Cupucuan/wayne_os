#!/bin/sh
# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# FILE:				cros_sample_crossystem_test.sh
#
# DESCRIPTION: Tests crossystem command. Basic tests to demonstrate a cros
#							 test implemented as a bash script.
#
# AUTHOR:			 truty@chromium.org
#
# FULL REFERENCE:
#		http://ltp.sourceforge.net/documentation/how-to/ltp.php
#		Contents and organization of LTP.
#		Instructions for creating tests including:
#		-.c test template
#		-Bash test template
#
# QUICK REFERENCE FOR TEST WRITING:
# 1. Create a new test subdirectory with Makefile based on this one.
# 2. Exporting TST_TOTAL, TCID and TST_COUNT are required. Copy this setup().
# 3. Use $LTPTMP/... for storing files produced.
# 4. Clean the files up in cleanup() if possible.
# 5. Return 0 for success and nonzero for fail.
# 6. Use these command line api's to indicate test status:
#
#		 Informational messages:
#			 tst_resm TINFO "Informational message..."
#
#		 PASSED test:
#			 tst_resm TPASS "Test succeeded message..."
#
#		 FAILED test:
#			 tst_resm TFAIL "Test failed message"
#
#		 Unexpected infrastructure problem:
#			 tst_brkm TBROK <cleanup_fn or NULL> "Unexpected ... message"
#
#		 Flavors of these commands exist that can also emit a file to include
#		 detailed status as follows:
#			 tst_res <status> <fname> "tmessage..."
#			 tst_brk TBROK <fname> <cleanup_fn> "tmessage..."
#
#			 <tstatus> is one of:
#				 TINFO - informational message
#				 TPASS - test passed
#				 TFAIL - test failed
#				 TWARN - warning during the test
# 7. Add your test to $LTPROOT/runtest/cros_kernel_unittests
# 8. Modify INSTALL_TARGETS in the Makefile to match the new script.


setup()
{
	# Required by LTP command line harness APIs:
	export TST_TOTAL=3	# Total number of test cases in this file.
	export TCID="setup" # Test case identifier
	export TST_COUNT=0	# Set up is initialized as test 0
	LTPTMP=${TMP}				# Temporary directory to create files, etc.

	#trap "cleanup" 0

	return 0
}


#cleanup()
#{
#  # Possible cleanup under $LTPTMP/... using rm -rf ...
#}


cros_cmd_cs01()
{
	# Test if crossystem command succeeds on this system.
	TCID="cros_sample_cmd_cs01"
	TST_COUNT=1
	RC=0

	tst_resm TINFO "Test #1: check if crossystem is available"
	crossystem >/dev/null || RC=$?
	if [ $RC -ne 0 ]; then
		tst_resm TFAIL "Unable to execute crossystem"
	else
		tst_resm TPASS "crossystem succeeded"
	fi
	return $RC
}


cros_cmd_cs02()
{
	# Test if crossystem command handles invalid commands properly.
	TCID="cros_sample_cmd_cs02"
	TST_COUNT=2
	RC=0

	tst_resm TINFO "Test #2: check if crossystem fails on invalid commands"
	crossystem invalidcommand >/dev/null 2>&1 || RC=$?
	if [ $RC -ne 1 ]; then
		tst_resm TFAIL "crossystem should have failed"
		RC=1
	else
		tst_resm TPASS "crossystem handled invalid command properly"
		RC=0
	fi
	return $RC
}


cros_cmd_cs03()
{
	# Test if one crossystem command returns valid values.
	TCID="cros_sample_cmd_cs03"
	TST_COUNT=3
	RC=0

	tst_resm TINFO "Test #3: check if crossystem returns valid data"
	SLOT=$(crossystem mainfw_act) || RC=$?
	if [ $RC -ne 0 ]; then
		tst_resm TFAIL "crossystem mainfw_act failed unexpectedly"
	else
		if [ $SLOT != 'A' ] && [ $SLOT != 'B' ]; then
			tst_resm TFAIL "crossystem mainfw_act failed unexpectedly"
		else
			tst_resm TPASS "crossystem mainfw"
		fi
	fi
	return $RC
}


# Function:			main
#
# Description:	- Execute all tests, exit with test status.
#
# Exit:					- zero on success
#								- non-zero on failure.
#
RC=0		# Return value from setup, and test functions.

setup || exit $RC

cros_cmd_cs01 || exit $RC
cros_cmd_cs02 || exit $RC
cros_cmd_cs03 || exit $RC
