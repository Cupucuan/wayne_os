#!/bin/sh
# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# FILE:				cros_unittests_wrapper.sh
#
# DESCRIPTION: Runs cros unittests programs.
#
# AUTHOR:			 truty@chromium.org
#

MANIFEST=./cros_kernel_unittests.manifest


setup()
{
	# Required by LTP command line harness APIs:
	export TST_TOTAL=$(wc -l < "${MANIFEST}")
	export TCID="setup" # Test case identifier
	export TST_COUNT=0	# Set up is initialized as test 0
}


run_unittest()
{
	# Run unittest with name supplied by $1
	local test_name=$1
	TCID="${test_name}"
	TST_COUNT=$(( TST_COUNT += 1 ))

	tst_resm TINFO "Test #${TST_COUNT}: ${test_name}"
	./${test_name}
	if [ $? -ne 0 ]; then
		tst_resm TFAIL "Test ${test_name} failed."
		return 1
	fi
	tst_resm TPASS "Test ${test_name} succeeded"
	return 0
}


# Function:			main
#
# Description:	- Execute all tests, exit with test status.
#
# Exit:					- zero on success
#								- non-zero on failure.
#
setup || exit $?

RC=0
while read test; do
	run_unittest "${test}"
	: $(( RC |= $? ))
done < "${MANIFEST}"
exit ${RC}
