#!/usr/bin/python
# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Entry point for running Whining Matrix dashboard on cmdline.

Runs an http server via bottle library's run() method.
"""
import logging, argparse, os, socket, sys

# Point the db properly.
from src import settings

# Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
# It enables simple webpage rendering from a single file (bottle.py) with no
# other dependencies.
# Until a proper repo is identified where bottle.py may reside, download
# and install a recent copy.
try:
    import bottle
except ImportError:
    print 'Bottle library not found. Please install from http://bottlepy.org.'
    print 'You can use:'
    if not os.path.exists('/etc/debian_chroot'):
        # Outside chroot
        print '$ sudo apt-get install python-setuptools'
    print '$ sudo easy_install bottle'
    sys.exit(1)

from src import urls


# Establish base directory
base_cmd = os.path.realpath(sys.argv[0])
base_dir = os.path.dirname(base_cmd)

# Handle command line arguments.
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--config', help='Configuration file',
                    dest='config', default=os.path.join(base_dir, 'config.ini'))
parser.add_argument('--run-debug',
                    help='Enable host debug messages',
                    dest='run_debug', action='store_true', default=False)
parser.add_argument('--run-host',
                    help='Supply host, e.g. w.x.y.z',
                    dest='run_host', default=socket.gethostname())
parser.add_argument('--run-port',
                    help='Supply host port',
                    dest='run_port', type=int, default=8080)
down_help = 'Return static/down.html for any URL, useful when system is broken.'
parser.add_argument('--down',
                    help=down_help,
                    dest='down', action='store_true', default=False)

options = parser.parse_args()

# Establish bases for templates and static.
templates_dir = os.path.join(base_dir, 'templates')
static_root = os.path.join(base_dir, 'static')
bottle.TEMPLATE_PATH.append(templates_dir)

app = bottle.Bottle()

# Map routes onto the app object.
# If the --down param was set, route everything to static/down.html
if options.down:
    @app.error(404)
    def route_system_down_message(code):
        return bottle.static_file('down.html', static_root)
else:
    urls.add_routes(app, static_root)

settings.settings = settings.Settings(options.config)

logging_level = logging.INFO
if options.run_debug:
    logging_level = logging.DEBUG
    print 'Serving with "debug" enabled.'
logging.basicConfig(level=logging_level)

# If a relative_root is set, mount wmatrix under that directory (for testing)
if settings.settings.relative_root != '':
    subapp = app
    app = bottle.Bottle()
    app.mount(settings.settings.relative_root, subapp)

# Run the http server that serves the local dashboard.
bottle.run(app=app, host=options.run_host, port=options.run_port,
           debug=options.run_debug, reloader=True)
