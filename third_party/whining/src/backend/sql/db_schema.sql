# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

/*
DB structure for the Whining Matrix dashboard.

single char abbreviations:
p - platform
b - build
s - suite
r - release_number
t - test
f - frequency, one of ('weekly', 'nightly', 'new_build')

so an "rpbs" index will be an index on (release_number, platform, build, suite)

Tables with import_ prefix are used by the importing scripts for staging new
data. The main table is "tests".

On import, newly seen suite runs are added to check_missing_* tables with a
future timestamp of when they should be checked for missing tests/suites/builds.

suite_stats and stats_rbsp tables are used as intermediate aggregations.
*/

############ Some settings first ###############################################
SET storage_engine=InnoDB;

############ Log table, MyISAM for avoiding locks and slightly better speed. ###
DROP TABLE IF EXISTS msg_log;
CREATE TABLE msg_log (
  msg_idx INT UNSIGNED NOT NULL AUTO_INCREMENT,
  msg_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  msg varchar(512) DEFAULT NULL,
  PRIMARY KEY (msg_idx)
) ENGINE=MyISAM;



########## Dictionary tables ###################################################
DROP TABLE IF EXISTS test_names;
CREATE TABLE test_names (
  test_name_idx INT UNSIGNED NOT NULL AUTO_INCREMENT,
  test_name VARCHAR(300) NOT NULL,
  PRIMARY KEY (test_name_idx),
  UNIQUE KEY key_test_name (test_name)
);

DROP TABLE IF EXISTS suites;
CREATE TABLE suites (
  suite_idx SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  suite VARCHAR(50) NOT NULL,
  PRIMARY KEY (suite_idx),
  UNIQUE KEY key_suite (suite)
);


# Configs refer to buildbot configuration names as defined in
# chromite/cbuildbot/cbuildbot_config.py
# (https://chromium.googlesource.com/chromiumos/chromite.git/+/
#     master/cbuildbot/cbuildbot_config.py)
#
# Examples:
# - x86-zgb-release
# - daisy-release
# - lumpy-pgo-release
DROP TABLE IF EXISTS configs;
CREATE TABLE configs (
  config_idx SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  config VARCHAR(50) NOT NULL,
  PRIMARY KEY (config_idx),
  UNIQUE KEY key_config (config)
);

DROP TABLE IF EXISTS platforms;
CREATE TABLE platforms (
  platform_idx SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  platform VARCHAR(20) NOT NULL,
  PRIMARY KEY (platform_idx),
  UNIQUE KEY key_platform (platform)
);


DROP TABLE IF EXISTS builds;
CREATE TABLE builds (
  build_idx INT UNSIGNED NOT NULL AUTO_INCREMENT,
  build VARCHAR(50) NOT NULL,
  first_seen_time DATETIME NOT NULL,
  PRIMARY KEY (build_idx),
  UNIQUE KEY key_build (build)
);


# Table tko_status represents the following dictionary:
#    {1: 'NOSTATUS',
#     2: 'ERROR',
#     3: 'ABORT',
#     4: 'FAIL',
#     5: 'WARN',
#     6: 'GOOD',
#     7: 'ALERT',
#     8: 'TEST_NA',
#     9: 'RUNNING'}
DROP TABLE IF EXISTS tko_status;
CREATE TABLE tko_status (
  status_idx TINYINT UNSIGNED NOT NULL PRIMARY KEY,
  word varchar(10) NOT NULL,
  UNIQUE KEY key_word (word)
);


###### Table good_tests, row example: ##########################################
#             test_idx: 78456598
#              job_idx: 2636750
#           afe_job_id: 2641773
#        test_name_idx: 27
#           status_idx: 6
#          machine_idx: 2637
#       release_number: 28
#            build_idx: 678
#            suite_idx: 12
#         platform_idx: 7
#           config_idx: 2
#      is_experimental: 0
#      job_queued_time: 2013-04-22 02:18:51
#    test_started_time: 2013-04-22 06:19:50
#   test_finished_time: 2013-04-22 06:22:50
#               reason: completed successfully
#             job_name: daisy-release/R28-4034.0.0-try
#              invalid: 0
# invalidates_test_idx: 78456598
#  retry_orig_test_idx: 78456597
#        fw_rw_version: daisy-firmware/R21-2534.0.0
#        fw_ro_version: NULL
#         test_version: daisy-release/R28-4034.0.0
DROP TABLE IF EXISTS good_tests;
CREATE TABLE good_tests (
  test_idx INT UNSIGNED NOT NULL,
  job_idx INT UNSIGNED NOT NULL,
  afe_job_id INT NOT NULL,
  test_name_idx INT UNSIGNED NOT NULL,
  status_idx TINYINT UNSIGNED NOT NULL,
  machine_idx INT UNSIGNED NOT NULL,
  release_number SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  config_idx SMALLINT UNSIGNED NOT NULL,
  is_experimental TINYINT NOT NULL DEFAULT 0,
  job_queued_time DATETIME NOT NULL,
  test_started_time DATETIME NOT NULL,
  test_finished_time DATETIME DEFAULT NULL,
  reason varchar(4096) DEFAULT NULL,
  job_name varchar(200) DEFAULT NULL,
  invalid TINYINT DEFAULT 0,
  invalidates_test_idx INT UNSIGNED,
  retry_orig_test_idx INT UNSIGNED,
  fw_rw_version varchar(50) DEFAULT NULL,
  fw_ro_version varchar(50) DEFAULT NULL,
  test_version varchar(50) DEFAULT NULL,
  PRIMARY KEY (test_idx),
  KEY key_tbp (test_name_idx, build_idx, platform_idx),
  KEY key_rsbpt_good (
    release_number, suite_idx, build_idx, platform_idx,
    test_name_idx, is_experimental, test_started_time),
  KEY key_invalidates_test_idx (invalidates_test_idx),
  KEY key_retry_orig_test_idx (retry_orig_test_idx),
  KEY key_faft (fw_rw_version, fw_ro_version, test_version)
);

DROP TABLE IF EXISTS bad_tests;
CREATE TABLE bad_tests LIKE good_tests;
ALTER TABLE bad_tests DROP KEY key_rsbpt_good;
# The last 4 fields in this index are rarely used for search but they can
# speed up retrieval for some queries because no access to table data is needed
# all the required fields are in the index, see "Using index" in extra_info
# http://dev.mysql.com/doc/refman/5.5/en/explain-output.html
ALTER TABLE bad_tests ADD KEY key_rsbpt_bad (
  release_number, suite_idx, build_idx, platform_idx, test_name_idx,
  status_idx, is_experimental, test_started_time);


###### Table images, row example: ##############################################
#           image_idx: 100
#      release_number: 25
#           build_idx: 86
#        platform_idx: 3
#          config_idx: 5
#       buildbot_root: http://example.com/i/chromiumos.release
#        builder_name: link full release-R25-3428.B
#               build: None
#               board: link
#              number: 93
#           completed: 1
#              result: 0
#   simplified_result: 1
#          start_time: 2013-02-03 07:00:02
#            end_time: 2013-02-03 09:51:57
#           chromever: 25.0.1364.67
#              reason: The Nightly scheduler named 'release release-R25-3428.B'
#                      triggered this build
DROP TABLE IF EXISTS images;
CREATE TABLE images (
  image_idx INT UNSIGNED NOT NULL AUTO_INCREMENT,
  release_number SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED DEFAULT NULL,
  config_idx SMALLINT UNSIGNED NOT NULL,
  buildbot_root varchar(511) DEFAULT NULL,
  builder_name varchar(100) DEFAULT NULL,
  build varchar(100) DEFAULT NULL,
  board varchar(45) DEFAULT NULL,
  `number` INT DEFAULT NULL,
  completed tinyint DEFAULT NULL,
  result tinyint DEFAULT NULL,
  simplified_result tinyint DEFAULT NULL,
  start_time datetime NOT NULL,
  end_time datetime DEFAULT NULL,
  chromever varchar(100) DEFAULT NULL,
  reason varchar(2000) DEFAULT NULL,
  PRIMARY KEY (image_idx),
  UNIQUE KEY key_rpb_num (release_number, platform_idx, build_idx, `number`)
);

# import table for build info harvested from the buildbots
# TODO (kamrik): remove unused columns from here.
DROP TABLE IF EXISTS import_images;
CREATE TABLE import_images (
  buildbot_root varchar(511) DEFAULT NULL,
  builder_name varchar(100) DEFAULT NULL,
  buildname varchar(100) DEFAULT NULL,
  build varchar(100) DEFAULT NULL,
  release_number int DEFAULT NULL,
  config varchar(45) DEFAULT NULL,
  board varchar(45) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  completed tinyint DEFAULT NULL,
  result int DEFAULT NULL,
  simplified_result tinyint DEFAULT NULL,
  start_time datetime DEFAULT NULL,
  end_time datetime DEFAULT NULL,
  chromever varchar(100) DEFAULT NULL,
  reason varchar(2000) DEFAULT NULL,
  platform varchar(100) DEFAULT NULL,
  revision varchar(100) DEFAULT NULL,
  build_major int DEFAULT NULL,
  build_minor int DEFAULT NULL,
  build_maintenance int DEFAULT NULL,
  build_remaining varchar(45) DEFAULT NULL
);


###### Table suite_schedule, imported from suite_scheduler.ini. Row example: ###
#       suite_idx: 29
#       rule_name: LinkPerf
#           suite: link_perf
#          run_on: new_build
#    branch_specs: >=R23
#            pool: link_perf
#   runs_per_week: 28
#             src: scheduler
DROP TABLE IF EXISTS suite_schedule;
CREATE TABLE suite_schedule (
  suite_idx SMALLINT UNSIGNED DEFAULT NULL,
  rule_name varchar(60) NOT NULL,
  suite varchar(60) NOT NULL,
  run_on varchar(45) DEFAULT NULL,
  branch_specs varchar(100) DEFAULT NULL,
  pool varchar(45) DEFAULT NULL,
  runs_per_week int DEFAULT NULL,
  src varchar(45) DEFAULT NULL,
  PRIMARY KEY (suite_idx, rule_name)
);

DROP TABLE IF EXISTS known_tests;
CREATE TABLE known_tests (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  test_name_idx INT UNSIGNED NOT NULL,
  PRIMARY KEY (release_number, platform_idx, suite_idx, test_name_idx)
);

# Build info from buildbots does not contain the platform name, we only know the
# builder configuration name like 'daisy-release' that has to be translated to
# platform name used in the AutoTest DB. 'daisy-release' -> 'snow'.
DROP TABLE IF EXISTS platform_configs;
CREATE TABLE platform_configs (
  config_idx SMALLINT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (config_idx, platform_idx)
);

DROP TABLE IF EXISTS suite_stats;
CREATE TABLE suite_stats (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  avg_duration double DEFAULT NULL,
  std_duration double DEFAULT NULL,
  latest_build_idx INT UNSIGNED NOT NULL,
  first_seen_time datetime DEFAULT NULL,
  last_seen_time datetime DEFAULT NULL,
  runs_total int DEFAULT NULL,
  days_observed int DEFAULT NULL,
  runs_per_week double DEFAULT NULL,
  run_on varchar(45) NOT NULL,
  PRIMARY KEY (suite_idx, platform_idx, release_number)
);

# Aggregated info on each suite run
DROP TABLE IF EXISTS stats_rpsb;
CREATE TABLE stats_rpsb (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  last_finished_time datetime DEFAULT NULL,
  first_started_time datetime DEFAULT NULL,
  seconds_duration INT DEFAULT NULL,
  was_running tinyint DEFAULT NULL,
  PRIMARY KEY (release_number, platform_idx, suite_idx, build_idx)
);


###### Table tko_machines, copied as is from the AutoTest DB. Row example: #####
#     machine_idx: 2399
#        hostname: 192.168.231.100
#   machine_group: lumpy
#           owner: None
DROP TABLE IF EXISTS tko_machines;
CREATE TABLE tko_machines (
  machine_idx int unsigned NOT NULL DEFAULT '0',
  hostname varchar(700) DEFAULT NULL,
  machine_group varchar(80) DEFAULT NULL,
  owner varchar(80) DEFAULT NULL,
  PRIMARY KEY (machine_idx)
);


############ Tables for import script ################
# TODO: drop PRIMARY keys for some of the import tables

drop table if exists import_tko_machines;
create table import_tko_machines like tko_machines;

drop table if exists import_tko_status;
create table import_tko_status like tko_status;

drop table if exists import_suite_schedule;
create table import_suite_schedule like suite_schedule;

#TODO: import_iteration_result

###### Table import_jobs, row example: #########################################
#             job_idx: 2636720
#                 tag: 2643023-moblab/192.168.231.100
#               label: lumpy-release/R28-4034.0.0/kernel_daily_regression
#                      /EmptyLines
#            username: moblab
#         machine_idx: 2391
#         queued_time: 2013-04-22 04:05:08
#        started_time: 2013-04-22 06:12:09
#       finished_time: 2013-04-22 06:14:33
#          afe_job_id: 2643023
#        build_config: lumpy-release
#           buildname: R28-4034.0.0
#               suite: kernel_daily_regression
#            test_lbl: EmptyLines
#      release_number: 28
#         build_major: 4034
#         build_minor: 0
#   build_maintenance: 0
#     build_remaining:
#               build: R00028-04034.00000.00000 (unused)
#   afe_parent_job_id: 2643020
#   build_in_tko_jobs: lumpy-release/R28-4034.0.0
#       build_version: R28-4034.0.0
#   suite_in_tko_jobs: kernel_daily_regression
#               board: lumpy
#       fw_rw_version: lumpy-firmware/R21-2534.0.0
#       fw_ro_version: null
#        test_version: lumpy-release/R28-4034.0.0
# TODO (kamrik) remove 'buildname' and keep the build in 'build' field.
DROP TABLE IF EXISTS import_jobs;
CREATE TABLE import_jobs (
  job_idx int unsigned NOT NULL DEFAULT '0',
  tag varchar(100) DEFAULT NULL,
  label varchar(100) DEFAULT NULL,
  username varchar(80) DEFAULT NULL,
  machine_idx int unsigned NOT NULL,
  queued_time datetime DEFAULT NULL,
  started_time datetime DEFAULT NULL,
  finished_time datetime DEFAULT NULL,
  afe_job_id int(11) DEFAULT NULL,
  build_config varchar(255) DEFAULT NULL,
  buildname varchar(255) DEFAULT NULL,
  suite varchar(255) DEFAULT NULL,
  test_lbl varchar(255) DEFAULT NULL,
  release_number int unsigned DEFAULT NULL,
  build_major int unsigned DEFAULT NULL,
  build_minor int unsigned DEFAULT NULL,
  build_maintenance int unsigned DEFAULT NULL,
  build_remaining varchar(30) DEFAULT NULL,
  build varchar(100) DEFAULT NULL,
  afe_parent_job_id int(11) DEFAULT NULL,
  build_in_tko_jobs varchar(255) DEFAULT NULL,
  build_version varchar(255) DEFAULT NULL,
  suite_in_tko_jobs varchar(40) DEFAULT NULL,
  board varchar(40) DEFAULT NULL,
  fw_rw_version varchar(50) DEFAULT NULL,
  fw_ro_version varchar(50) DEFAULT NULL,
  test_version varchar(50) DEFAULT NULL,
  PRIMARY KEY (job_idx)
);

# PRIMARY index must remain, or the view will be extremely slow

DROP TABLE IF EXISTS import_tko_tests;
CREATE TABLE import_tko_tests (
  test_idx int(10) unsigned NOT NULL,
  job_idx int(10) unsigned NOT NULL,
  test varchar(300) DEFAULT NULL,
  subdir varchar(300) DEFAULT NULL,
  kernel_idx int(10) unsigned NOT NULL,
  status int(10) unsigned NOT NULL,
  reason varchar(4096) DEFAULT NULL,
  machine_idx int(10) unsigned NOT NULL,
  invalid tinyint(1) DEFAULT '0',
  finished_time datetime DEFAULT NULL,
  started_time datetime DEFAULT NULL,
  invalidates_test_idx int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (test_idx)
);


############ Table used for intermediate calculations ##########################
#TODO: (maybe make it a tmp, not sure if index is helpful here)
DROP TABLE IF EXISTS upd_suite_runs;
CREATE TABLE upd_suite_runs (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  max_started_time datetime DEFAULT NULL,
  max_finished_time datetime DEFAULT NULL,
  PRIMARY KEY (release_number, platform_idx, suite_idx, build_idx)
);



############ Tables for calculating missing tests/suites/builds ################
DROP TABLE IF EXISTS check_missing_tests;
CREATE TABLE check_missing_tests (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  queued_time datetime,
  expected_time datetime,
  check_after_time datetime,
  PRIMARY KEY (release_number, platform_idx, suite_idx, build_idx),
  KEY key_check_after_time (check_after_time)
);


DROP TABLE IF EXISTS check_missing_suites;
CREATE TABLE check_missing_suites (
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  run_on varchar(10) NOT NULL,
  queued_time datetime,
  expected_time datetime,
  check_after_time datetime DEFAULT NULL,
  PRIMARY KEY (release_number, platform_idx, build_idx, run_on)
);


DROP TABLE IF EXISTS missing_tests;
CREATE TABLE missing_tests (
  missing_test_idx INT unsigned NOT NULL AUTO_INCREMENT,
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  test_name_idx INT UNSIGNED NOT NULL,
  expected_time datetime,
  detected_time datetime NOT NULL,
  PRIMARY KEY (missing_test_idx),
  UNIQUE KEY key_rspbt (
    release_number, suite_idx, platform_idx, build_idx, test_name_idx),
  KEY key_bsr (build_idx, suite_idx, release_number)
);


DROP TABLE IF EXISTS missing_suites;
CREATE TABLE missing_suites (
  missing_suite_idx INT unsigned NOT NULL AUTO_INCREMENT,
  release_number INT UNSIGNED NOT NULL,
  platform_idx SMALLINT UNSIGNED NOT NULL,
  build_idx INT UNSIGNED NOT NULL,
  suite_idx SMALLINT UNSIGNED NOT NULL,
  expected_time datetime,
  detected_time datetime NOT NULL,
  PRIMARY KEY (missing_suite_idx),
  UNIQUE KEY key_rspb (release_number, suite_idx, platform_idx, build_idx)
);


DROP TABLE IF EXISTS triage_comments;
CREATE  TABLE triage_comments (
    comment_idx INT unsigned NOT NULL AUTO_INCREMENT ,
    test_idx INT unsigned NOT NULL ,
    replaced_by INT unsigned DEFAULT NULL ,
    created_time DATETIME NOT NULL ,
    comment VARCHAR(2048) DEFAULT NULL ,
    test_name_idx INT UNSIGNED NOT NULL,
    suite_idx SMALLINT UNSIGNED NOT NULL,
    build_idx INT UNSIGNED NOT NULL,
    release_number INT UNSIGNED NOT NULL,
    platform_idx SMALLINT UNSIGNED NOT NULL,
    status_idx TINYINT NOT NULL,
    reason VARCHAR(4096) DEFAULT NULL,
    PRIMARY KEY (comment_idx),
    KEY key_test_idx (test_idx),
    KEY key_test_name (test_name_idx)
);

# Comments for missing tests
DROP TABLE IF EXISTS mt_comments;
CREATE  TABLE mt_comments (
    mt_comment_idx INT unsigned NOT NULL AUTO_INCREMENT ,
    missing_test_idx INT unsigned NOT NULL ,
    replaced_by INT unsigned DEFAULT NULL ,
    created_time DATETIME NOT NULL ,
    comment VARCHAR(2048) DEFAULT NULL ,
    test_name_idx INT UNSIGNED NOT NULL,
    suite_idx SMALLINT UNSIGNED NOT NULL,
    build_idx INT UNSIGNED NOT NULL,
    release_number INT UNSIGNED NOT NULL,
    platform_idx SMALLINT UNSIGNED NOT NULL,
    PRIMARY KEY (mt_comment_idx) ,
    KEY key_missing_test_idx (missing_test_idx)
);

# Comments for missing suites
DROP TABLE IF EXISTS ms_comments;
CREATE  TABLE ms_comments (
    ms_comment_idx INT unsigned NOT NULL AUTO_INCREMENT ,
    missing_suite_idx INT unsigned NOT NULL ,
    replaced_by INT unsigned DEFAULT NULL ,
    created_time DATETIME NOT NULL ,
    comment VARCHAR(2048) DEFAULT NULL ,
    suite_idx SMALLINT UNSIGNED NOT NULL,
    build_idx INT UNSIGNED NOT NULL,
    release_number INT UNSIGNED NOT NULL,
    platform_idx SMALLINT UNSIGNED NOT NULL,
    PRIMARY KEY (ms_comment_idx) ,
    KEY key_missing_suite_idx (missing_suite_idx)
);


# A table to hold the time we wait before declaring a test or suite missing
# depending on the type of test - weekly, nightly, per-build.
DROP TABLE IF EXISTS alert_age;
create table alert_age (
     run_on varchar(10) DEFAULT NULL,
     test_alert_hours int DEFAULT 2,
     suite_alert_hours int DEFAULT 5,
     build_alert_hours int DEFAULT 8
);

insert into alert_age (
  run_on, test_alert_hours, suite_alert_hours, build_alert_hours)
VALUES ('new_build', 2, 5, 8),
    ('nightly', 20, 24, 48),
    ('weekly', 24*3, 24*5, 25*7);

# Tables for auto-filed bugs.
DROP TABLE IF EXISTS autobugs;
CREATE TABLE autobugs (
  jkv_id int unsigned NOT NULL,
  test_idx int unsigned NOT NULL,
  bug_id int unsigned NOT NULL,
  PRIMARY KEY (test_idx, bug_id)
);

DROP TABLE IF EXISTS import_autobugs;
CREATE TABLE import_autobugs like autobugs;
