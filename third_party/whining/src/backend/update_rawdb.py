# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

""" Copy some of the data from AutoTest DB as is, to maintain our partial
copy of the AutoTest DB to simplify initialization of wmdb databases from it
after changes in wmdb schema.
"""


import time
import logging

from chromite.lib import metrics
from chromite.lib import ts_mon_config
from src import settings
from src.backend import dbpump
from src.backend import update_lib
from src.models import db_interface

SRC_DB  = settings.AUTOTEST_DB_NAME
DEST_DB = settings.RAW_DB_NAME


if __name__ == '__main__':
    with ts_mon_config.SetupTsMonGlobalState('update_rawdb',
                                          auto_flush=False):

        options = update_lib.initialize_options()

        settings.settings, src_db, dest_db = (
            update_lib.initialize_db_connection(SRC_DB, DEST_DB, options))

        result = None
        try:
            update_lib.wrapped_query(src_db, dest_db, raw=True,
                                     init_days=options.init_days)
        except Exception as error:
            result = error
            raise
        finally:
            update_lib.log_update(result, *update_lib.retrieve_dest_db_label(
                settings.settings, DEST_DB))
            metrics.Flush()
            # Cleanup
            src_db.close()
            dest_db.close()
