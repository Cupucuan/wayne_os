# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

""" Update or initialize a dashboard database with new data taken from our
partial copy of AutoTest DB.
"""


import time
import logging

from chromite.lib import metrics
from chromite.lib import ts_mon_config
from infra_libs import ts_mon
from src import settings
from src.backend import dbpump
from src.backend import update_lib
from src.models import db_interface

SRC_DB  = settings.RAW_DB_NAME
DEST_DB = settings.WMATRIX_DB_NAME


if __name__ == '__main__':
    with ts_mon_config.SetupTsMonGlobalState('update_wmdb',
                                          auto_flush=False):

        options = update_lib.initialize_options()

        settings.settings, src_db, dest_db = (
            update_lib.initialize_db_connection(SRC_DB, DEST_DB, options))

        result = None
        try:
            update_lib.wrapped_query(src_db, dest_db, raw=False,
                                     init_days=options.init_days)

            logging.info('Updating retry information.')
            start_time = time.time()
            dest_db.run_query_cmd('CALL update_retry_info()')
            logging.info('update_retry_info() took %0.3f seconds',
                         time.time() - start_time)
        except Exception as error:
            result = error
            raise
        finally:
            update_lib.log_update(result, *update_lib.retrieve_dest_db_label(
                settings.settings, DEST_DB))
            metrics.Flush()
            # Cleanup
            src_db.close()
            dest_db.close()
